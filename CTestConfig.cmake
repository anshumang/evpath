SET(CTEST_PROJECT_NAME "Evpath")
SET(CTEST_NIGHTLY_START_TIME "17:9:39 GMT")

IF(NOT DEFINED CTEST_DROP_METHOD)
  SET(CTEST_DROP_METHOD "http")
ENDIF(NOT DEFINED CTEST_DROP_METHOD)

IF(CTEST_DROP_METHOD STREQUAL "http")
  SET(CTEST_DROP_SITE "cdash.cercs.gatech.edu")
  SET(CTEST_DROP_LOCATION "/submit.php?project=Evpath")
  SET(CTEST_TRIGGER_SITE "cdash.cercs.gatech.edu")
ENDIF(CTEST_DROP_METHOD STREQUAL "http")

set (MEMORYCHECK_SUPPRESSIONS_FILE "/opt/data1/share/users/anshuman/build_area/evpath/source/evpath.supp" CACHE FILEPATH "suppressions file" FORCE)
